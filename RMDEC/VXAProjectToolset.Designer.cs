﻿namespace RMDEC
{
    partial class vxaProjectToolset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(vxaProjectToolset));
            this.encryptedFileList = new System.Windows.Forms.ListBox();
            this.decryptSelected = new System.Windows.Forms.Button();
            this.decryptedFileList = new System.Windows.Forms.ListBox();
            this.processingBar = new System.Windows.Forms.ProgressBar();
            this.processingText = new System.Windows.Forms.Label();
            this.makeVXA = new System.Windows.Forms.Button();
            this.layout1 = new System.Windows.Forms.TableLayoutPanel();
            this.layout1.SuspendLayout();
            this.SuspendLayout();
            // 
            // encryptedFileList
            // 
            this.encryptedFileList.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.encryptedFileList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.encryptedFileList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.encryptedFileList.FormattingEnabled = true;
            this.encryptedFileList.HorizontalScrollbar = true;
            this.encryptedFileList.Location = new System.Drawing.Point(3, 3);
            this.encryptedFileList.Name = "encryptedFileList";
            this.encryptedFileList.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.encryptedFileList.Size = new System.Drawing.Size(820, 141);
            this.encryptedFileList.TabIndex = 3;
            // 
            // decryptSelected
            // 
            this.decryptSelected.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.decryptSelected.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.decryptSelected.Enabled = false;
            this.decryptSelected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.decryptSelected.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.decryptSelected.Location = new System.Drawing.Point(389, 156);
            this.decryptSelected.Name = "decryptSelected";
            this.decryptSelected.Size = new System.Drawing.Size(48, 48);
            this.decryptSelected.TabIndex = 4;
            this.decryptSelected.Text = "|\r\nV";
            this.decryptSelected.UseVisualStyleBackColor = false;
            this.decryptSelected.Click += new System.EventHandler(this.decryptSelected_Click);
            // 
            // decryptedFileList
            // 
            this.decryptedFileList.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.decryptedFileList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.decryptedFileList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.decryptedFileList.FormattingEnabled = true;
            this.decryptedFileList.HorizontalScrollbar = true;
            this.decryptedFileList.Location = new System.Drawing.Point(3, 217);
            this.decryptedFileList.Name = "decryptedFileList";
            this.decryptedFileList.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.decryptedFileList.Size = new System.Drawing.Size(820, 143);
            this.decryptedFileList.TabIndex = 5;
            // 
            // processingBar
            // 
            this.processingBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.processingBar.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.processingBar.Location = new System.Drawing.Point(154, 415);
            this.processingBar.Name = "processingBar";
            this.processingBar.Size = new System.Drawing.Size(684, 22);
            this.processingBar.TabIndex = 7;
            // 
            // processingText
            // 
            this.processingText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.processingText.BackColor = System.Drawing.Color.Red;
            this.processingText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.processingText.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.processingText.Location = new System.Drawing.Point(12, 415);
            this.processingText.Name = "processingText";
            this.processingText.Size = new System.Drawing.Size(136, 22);
            this.processingText.TabIndex = 8;
            this.processingText.Text = "Waiting";
            this.processingText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // makeVXA
            // 
            this.makeVXA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.makeVXA.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.makeVXA.Enabled = false;
            this.makeVXA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.makeVXA.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.makeVXA.Location = new System.Drawing.Point(18, 385);
            this.makeVXA.Name = "makeVXA";
            this.makeVXA.Size = new System.Drawing.Size(820, 24);
            this.makeVXA.TabIndex = 10;
            this.makeVXA.Text = "Decrypt and Uncompress to VX Ace Project";
            this.makeVXA.UseVisualStyleBackColor = false;
            this.makeVXA.Click += new System.EventHandler(this.makeVXA_Click);
            // 
            // layout1
            // 
            this.layout1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layout1.ColumnCount = 1;
            this.layout1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout1.Controls.Add(this.encryptedFileList, 0, 0);
            this.layout1.Controls.Add(this.decryptedFileList, 0, 2);
            this.layout1.Controls.Add(this.decryptSelected, 2, 1);
            this.layout1.Location = new System.Drawing.Point(15, 12);
            this.layout1.Name = "layout1";
            this.layout1.RowCount = 3;
            this.layout1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.76087F));
            this.layout1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.47826F));
            this.layout1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.76087F));
            this.layout1.Size = new System.Drawing.Size(826, 363);
            this.layout1.TabIndex = 7;
            // 
            // vxaProjectToolset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(853, 442);
            this.Controls.Add(this.makeVXA);
            this.Controls.Add(this.processingText);
            this.Controls.Add(this.processingBar);
            this.Controls.Add(this.layout1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(869, 481);
            this.Name = "vxaProjectToolset";
            this.Text = "RMDec - RPG Maker VX ACE";
            this.Load += new System.EventHandler(this.VXAProjectToolset_Load);
            this.layout1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListBox encryptedFileList;
        private System.Windows.Forms.Button decryptSelected;
        private System.Windows.Forms.ListBox decryptedFileList;
        private System.Windows.Forms.ProgressBar processingBar;
        private System.Windows.Forms.Label processingText;
        private System.Windows.Forms.Button makeVXA;
        private System.Windows.Forms.TableLayoutPanel layout1;
    }
}
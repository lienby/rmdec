﻿namespace RMDEC
{
    partial class mvProjectToolset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mvProjectToolset));
            this.encryptedFileList = new System.Windows.Forms.ListBox();
            this.decryptSelected = new System.Windows.Forms.Button();
            this.decryptedFileList = new System.Windows.Forms.ListBox();
            this.encryptSelected = new System.Windows.Forms.Button();
            this.processingBar = new System.Windows.Forms.ProgressBar();
            this.processingText = new System.Windows.Forms.Label();
            this.makeMV = new System.Windows.Forms.Button();
            this.unmakeMV = new System.Windows.Forms.Button();
            this.layout1 = new System.Windows.Forms.TableLayoutPanel();
            this.layout2 = new System.Windows.Forms.TableLayoutPanel();
            this.layout1.SuspendLayout();
            this.layout2.SuspendLayout();
            this.SuspendLayout();
            // 
            // encryptedFileList
            // 
            this.encryptedFileList.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.encryptedFileList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.encryptedFileList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.encryptedFileList.FormattingEnabled = true;
            this.encryptedFileList.HorizontalScrollbar = true;
            this.encryptedFileList.Location = new System.Drawing.Point(3, 3);
            this.encryptedFileList.Name = "encryptedFileList";
            this.layout1.SetRowSpan(this.encryptedFileList, 4);
            this.encryptedFileList.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.encryptedFileList.Size = new System.Drawing.Size(334, 357);
            this.encryptedFileList.TabIndex = 3;
            // 
            // decryptSelected
            // 
            this.decryptSelected.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.decryptSelected.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.decryptSelected.Enabled = false;
            this.decryptSelected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.decryptSelected.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.decryptSelected.Location = new System.Drawing.Point(387, 98);
            this.decryptSelected.Name = "decryptSelected";
            this.decryptSelected.Size = new System.Drawing.Size(48, 48);
            this.decryptSelected.TabIndex = 4;
            this.decryptSelected.Text = "-->";
            this.decryptSelected.UseVisualStyleBackColor = false;
            this.decryptSelected.Click += new System.EventHandler(this.decryptSelected_Click);
            // 
            // decryptedFileList
            // 
            this.decryptedFileList.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.decryptedFileList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.decryptedFileList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.decryptedFileList.FormattingEnabled = true;
            this.decryptedFileList.HorizontalScrollbar = true;
            this.decryptedFileList.Location = new System.Drawing.Point(486, 3);
            this.decryptedFileList.Name = "decryptedFileList";
            this.layout1.SetRowSpan(this.decryptedFileList, 4);
            this.decryptedFileList.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.decryptedFileList.Size = new System.Drawing.Size(337, 357);
            this.decryptedFileList.TabIndex = 5;
            // 
            // encryptSelected
            // 
            this.encryptSelected.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.encryptSelected.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.encryptSelected.Enabled = false;
            this.encryptSelected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.encryptSelected.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.encryptSelected.Location = new System.Drawing.Point(387, 208);
            this.encryptSelected.Name = "encryptSelected";
            this.encryptSelected.Size = new System.Drawing.Size(48, 48);
            this.encryptSelected.TabIndex = 6;
            this.encryptSelected.Text = "<--";
            this.encryptSelected.UseVisualStyleBackColor = false;
            this.encryptSelected.Click += new System.EventHandler(this.encryptSelected_Click);
            // 
            // processingBar
            // 
            this.processingBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.processingBar.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.processingBar.Location = new System.Drawing.Point(154, 415);
            this.processingBar.Name = "processingBar";
            this.processingBar.Size = new System.Drawing.Size(684, 22);
            this.processingBar.TabIndex = 7;
            // 
            // processingText
            // 
            this.processingText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.processingText.BackColor = System.Drawing.Color.Red;
            this.processingText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.processingText.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.processingText.Location = new System.Drawing.Point(12, 415);
            this.processingText.Name = "processingText";
            this.processingText.Size = new System.Drawing.Size(136, 22);
            this.processingText.TabIndex = 8;
            this.processingText.Text = "Waiting";
            this.processingText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // makeMV
            // 
            this.makeMV.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.makeMV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.makeMV.Enabled = false;
            this.makeMV.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.makeMV.Location = new System.Drawing.Point(417, 3);
            this.makeMV.Name = "makeMV";
            this.makeMV.Size = new System.Drawing.Size(409, 24);
            this.makeMV.TabIndex = 9;
            this.makeMV.Text = "Un-Deploy MV Project";
            this.makeMV.UseVisualStyleBackColor = false;
            this.makeMV.Click += new System.EventHandler(this.makeMV_Click);
            // 
            // unmakeMV
            // 
            this.unmakeMV.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.unmakeMV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.unmakeMV.Enabled = false;
            this.unmakeMV.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.unmakeMV.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.unmakeMV.Location = new System.Drawing.Point(3, 3);
            this.unmakeMV.Name = "unmakeMV";
            this.unmakeMV.Size = new System.Drawing.Size(408, 24);
            this.unmakeMV.TabIndex = 10;
            this.unmakeMV.Text = "Deploy MV Project";
            this.unmakeMV.UseVisualStyleBackColor = false;
            this.unmakeMV.Click += new System.EventHandler(this.unmakeMV_Click);
            // 
            // layout1
            // 
            this.layout1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layout1.ColumnCount = 5;
            this.layout1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.17065F));
            this.layout1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.829354F));
            this.layout1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.layout1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.829354F));
            this.layout1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.17064F));
            this.layout1.Controls.Add(this.encryptSelected, 2, 2);
            this.layout1.Controls.Add(this.encryptedFileList, 0, 0);
            this.layout1.Controls.Add(this.decryptedFileList, 4, 0);
            this.layout1.Controls.Add(this.decryptSelected, 2, 1);
            this.layout1.Location = new System.Drawing.Point(15, 12);
            this.layout1.Name = "layout1";
            this.layout1.RowCount = 4;
            this.layout1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.89562F));
            this.layout1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 39.05724F));
            this.layout1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 38.04714F));
            this.layout1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.layout1.Size = new System.Drawing.Size(826, 363);
            this.layout1.TabIndex = 7;
            // 
            // layout2
            // 
            this.layout2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layout2.ColumnCount = 2;
            this.layout2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layout2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layout2.Controls.Add(this.unmakeMV, 0, 0);
            this.layout2.Controls.Add(this.makeMV, 1, 0);
            this.layout2.Location = new System.Drawing.Point(12, 380);
            this.layout2.Name = "layout2";
            this.layout2.RowCount = 1;
            this.layout2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layout2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layout2.Size = new System.Drawing.Size(829, 30);
            this.layout2.TabIndex = 11;
            // 
            // mvProjectToolset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(853, 442);
            this.Controls.Add(this.processingText);
            this.Controls.Add(this.processingBar);
            this.Controls.Add(this.layout1);
            this.Controls.Add(this.layout2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(869, 481);
            this.Name = "mvProjectToolset";
            this.Text = "RMDec - RPG Maker MV";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MVProjectToolset_FormClosing);
            this.Load += new System.EventHandler(this.MVProjectToolset_Load);
            this.layout1.ResumeLayout(false);
            this.layout2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListBox encryptedFileList;
        private System.Windows.Forms.Button decryptSelected;
        private System.Windows.Forms.ListBox decryptedFileList;
        private System.Windows.Forms.Button encryptSelected;
        private System.Windows.Forms.ProgressBar processingBar;
        private System.Windows.Forms.Label processingText;
        private System.Windows.Forms.Button makeMV;
        private System.Windows.Forms.Button unmakeMV;
        private System.Windows.Forms.TableLayoutPanel layout1;
        private System.Windows.Forms.TableLayoutPanel layout2;
    }
}
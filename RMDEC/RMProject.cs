﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMDEC
{
    
    class RMProject
    {
        public static Byte[] Xor(byte[] input, byte[] key)
        {
            long inpLen = input.LongLength;
            byte[] output = new byte[inpLen];

            for (long i = 0; i < input.LongLength; i++)
            {
                output[i] = (byte)(input[i] ^ key[i % key.LongLength]);
            }

            return output;
        }
    }
}

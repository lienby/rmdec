﻿namespace RMDEC
{
    partial class projectSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(projectSelector));
            this.projectList = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.projectDir = new System.Windows.Forms.TextBox();
            this.browseButton = new System.Windows.Forms.Button();
            this.selectButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // projectList
            // 
            this.projectList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projectList.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.projectList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.projectList.FormattingEnabled = true;
            this.projectList.Location = new System.Drawing.Point(12, 34);
            this.projectList.Name = "projectList";
            this.projectList.Size = new System.Drawing.Size(637, 392);
            this.projectList.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(9, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Project Directory:";
            // 
            // projectDir
            // 
            this.projectDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projectDir.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.projectDir.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.projectDir.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.projectDir.Location = new System.Drawing.Point(98, 8);
            this.projectDir.Name = "projectDir";
            this.projectDir.Size = new System.Drawing.Size(464, 20);
            this.projectDir.TabIndex = 2;
            this.projectDir.TextChanged += new System.EventHandler(this.projectDir_TextChanged);
            // 
            // browseButton
            // 
            this.browseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseButton.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.browseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.browseButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.browseButton.Location = new System.Drawing.Point(568, 8);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(81, 20);
            this.browseButton.TabIndex = 3;
            this.browseButton.Text = "Browse";
            this.browseButton.UseVisualStyleBackColor = false;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // selectButton
            // 
            this.selectButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.selectButton.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.selectButton.Enabled = false;
            this.selectButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.selectButton.Location = new System.Drawing.Point(12, 436);
            this.selectButton.Name = "selectButton";
            this.selectButton.Size = new System.Drawing.Size(637, 23);
            this.selectButton.TabIndex = 4;
            this.selectButton.Text = "Select RPG Maker Project";
            this.selectButton.UseVisualStyleBackColor = false;
            this.selectButton.Click += new System.EventHandler(this.selectButton_Click);
            // 
            // projectSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(661, 466);
            this.Controls.Add(this.projectList);
            this.Controls.Add(this.selectButton);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.projectDir);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(677, 505);
            this.Name = "projectSelector";
            this.Text = "RMDec - Project Selection";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.projectSelector_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox projectList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox projectDir;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.Button selectButton;
    }
}

